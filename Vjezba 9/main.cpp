#include "Vjezba 9.h"
#include <iostream>
#include <vector>
#include <algorithm>
#include <string>

int main()
{
	Pair<char*, char*> p1, p2, p3;
	std::vector<Pair<char*, char*>> v;

	std::cin >> p1 >> p2 >> p3;

	v.push_back(p1);
	v.push_back(p2);
	v.push_back(p3);

	sort(v.begin(), v.end());

	std::vector<Pair<char*, char*> >::iterator it;
	for (it = v.begin(); it != v.end(); ++it)
		std::cout << *it << std::endl;

}



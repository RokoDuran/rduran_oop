#pragma once
#include <iostream>
template<typename T1, typename T2>
class Pair
{
	T1 first;
	T2 second;
public:

	Pair(const T1& t1 = 0, const T1& t2 = 0) : first(t1), second(t2) {};
	Pair(const Pair<T1, T2>& other) {
		first = other.first;
		second = other.second;
	};
	bool operator== (const Pair<T1, T2>& other) const
	{
		return (first == other.first) && (second == other.second);
	};
	
	bool operator !=(const Pair<T1,T2>& other)const{
		return (first != other.first) && (second != other.second);
	}

	bool operator >(const Pair<T1, T2>& other)const {
		return (first > other.first) && (second > other.second);
	};

	bool operator <(const Pair<T1, T2>& other)const {
		return (first < other.first) && (second < other.second);
	};
	bool operator >=(const Pair<T1, T2>& other)const {
		return (first >= other.first) && (second > other.second);
	};
	bool operator <=(const Pair<T1, T2>& other)const {
		return first <= other.first && second <= other.second;
	};
	void operator=(const Pair<T1, T2>& other) {
		first = other.first;
		second = other.second;
	};
	friend std::ostream& operator << (std::ostream& os, Pair<T1, T2>& par) {
		os << "First: " << par.first << " Second: " << par.second << std::endl;
		return os;
	};
	friend std::istream& operator >> (std::istream& is, Pair<T1, T2>& par) {
		is >> par.first;
		is >> par.second;
		return is;
	};
	void swap(const Pair<T1,T2>& other) {
		T1 temp=first;
		T2 temp2=second;
		first = other.first;
		second = other.second;
		other.first = temp;
		other.second = temp2;
	};

};

template <>
class Pair<char*,char*> {
	 char * first;
	 char * second;
public:
	Pair() { first = new char; second = new char; }
	Pair(char * p, char *d) : first{ p }, second{ d }{};

	friend std::ostream& operator << (std::ostream& os, Pair<char*,char*>& par) {
		os << par.first <<par.second << std::endl;
		return os;
	};
	friend std::istream& operator >> (std::istream& is, Pair<char*,char*>& par) {
		is >> par.first;
		is >> par.second;
		return is;
	};
	bool operator >(const Pair<char*,char*>& other)const {
		return (first > other.first) && (second > other.second);
	};

	bool operator <(const Pair<char*,char*>& other)const {
		return (first < other.first) && (second < other.second);
	};

};
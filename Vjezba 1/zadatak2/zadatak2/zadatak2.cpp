#include "pch.h"
#include <iostream>
#include <string>

using namespace std;

struct Studenti
{
	char* ID;
	char* Ime;
	char* Prezime;
	char spol;
	int prvi_kviz, drugi_kviz, midterm_score, final_score, total_score ;
};
void dodaj(Studenti niz[], int* size)
{
	Studenti* novo;
	novo = (Studenti*)malloc(sizeof(Studenti));
	char* ID;
	ID = (char*)malloc(sizeof(char)*10);
	cout << "Unesite ID" << endl;
	cin >> ID;
	for (int i = 0; i < *size; i++)
	{
		if (!strcmp(ID,niz[i].ID))
		{
			free(ID);
			cout << "Vec postoji student s tim ID-om" << endl;
			return;
		}
	}
	novo->ID = ID;
	cout << "Unesite Ime" << endl;
	novo->Ime = (char*)malloc(sizeof(char)*10);
	cin >> novo->Ime;
	cout << "Unesite prezime" << endl;
	novo->Prezime = (char*)malloc(sizeof(char) * 10);
	cin >> novo->Prezime;
	cout << "Unesite spol (M/Z)" << endl;
	while (1)
	{
		char spol;
		cin >> spol;
		if (spol == 'Z' || spol == 'M')
		{
			novo->spol = spol;
			break;
		}
		else cout << "Krivi unos spola\n";
	}
	while (1)
	{
		int prvi, drugi, midterm, finalni;
		cout << "Unesite ocjene za prvi,drugi, midterm i  zavrsni." << endl;
		cin >> prvi >> drugi >> midterm >> finalni;
		if (prvi >= 0 || drugi >= 0 || midterm >= 0 || finalni >= 0)
		{
			novo->prvi_kviz = prvi;
			novo->drugi_kviz = drugi;
			novo->midterm_score = midterm;
			novo->final_score = finalni;
			novo->total_score = prvi + drugi + midterm + finalni;
			break;
		}
	}
	niz[*size] = *novo;
	*size=*size+1;
}

void ukloni(Studenti niz[], int* size)
{
	if (!*size) return;
	cout << "Unesite ID studenta za ukloniti." << endl;
	char ID[10];
	cin >> ID;
	for (int i = 0; i < *size; i++)
	{
		if (!strcmp(ID, niz[i].ID))
		{
			free(niz[i].ID);
			free(niz[i].Ime);
			free(niz[i].Prezime);
			for (int j = i; j < *size-1; j++)
			{
				niz[j] = niz[j + 1];
			}
			*size=*size-1;
				return;
		}
	}
	cout << "Ne postoji student s tim ID-om.";
	return;
}

void azuriraj(Studenti niz[], int size)
{
	if (!(size))
	{
		cout << "Nema se sto azurirati.";
		return;
	}
	int opcija;
	char ID[10];
	cout << "Unesite ID studenta?" << endl;
	cin >> ID;
	for (int i=0;i< size; i++)
	{	
		if (!strcmp(niz[i].ID, ID))
		{
			while (1)
			{
				cout << "Sto zelite azurirati?" << endl;
				cout << "Opcije :" << endl;
				cout << "1:ID  2: Ime/Prezime  3:  Spol  4:  Ocjene  5: Izlaz" << endl;
				cin >> opcija;
				if (opcija == 1)
				{
					int flag = 1;
					while (flag)
					{
						cout << "Unesite novi ID" << endl;
						char* novi_id;
						novi_id = (char*)malloc(sizeof(char) * 10);
						cin >> novi_id;
						for (int j = 0; j < size; j++)
						{
							if (!strcmp(novi_id, niz[j].ID))
							{
								cout << "Vec postoji student s tim ID-om";
								free(novi_id);
								break;
							}
							if (j == size - 1) {
								flag = 0;
								free(niz[i].ID);
							    niz[i].ID= novi_id;
							}
						}

					}

				}
				if (opcija == 2)
				{
					cout << "Ime:" << endl;
					cin >> niz[i].Ime;
					cout << "Prezime:" << endl;
					cin >> niz[i].Prezime;
				}

				if (opcija == 3)
				{
					cout << "Spol: " << endl;
					cin >> niz[i].spol;
				}

				if (opcija == 4)
				{
					cout << "Kviz 1:" << endl;
					cin >> niz[i].prvi_kviz;
					cout << "Kviz 2:" << endl;
					cin >> niz[i].drugi_kviz;
					cout << "Midterm:" << endl;
					cin >> niz[i].midterm_score;
					cout << "Finalni: " << endl;
					cin >> niz[i].final_score;
					niz[i].total_score = niz[i].prvi_kviz + niz[i].drugi_kviz + niz[i].midterm_score + niz[i].final_score;
				}

				if (opcija == 5)
					return;

			}
		}
	}
	cout << "Ne postoji student s tim ID-om.";
	return;
}
void prikazi(Studenti niz[], int size)
{
	if (!size) return;
	for (int i = 0; i < size; i++)
	{
		cout << "ID:  " << niz[i].ID << endl;
		cout << "Ime:  " << niz[i].Ime << endl;
		cout << "Prezime:  " << niz[i].Prezime << endl;
		cout << "Spol:  " << niz[i].spol << endl;
		cout << "Kviz1:  " << niz[i].prvi_kviz << endl;
		cout << "Kviz2:  " << niz[i].drugi_kviz << endl;
		cout << "Midterm: " << niz[i].midterm_score << endl;
		cout << "Finalni: " << niz[i].final_score << endl;
		cout << "Totalni: " << niz[i].total_score << endl;
	}

}
void prosjek(Studenti niz[], int size)
{
	if (!size) return;
	char ID[10];
	cout << "Unesite ID" << endl;
	cin >> ID;
	for (int i = 0; i < size; i++)
	{
		if (!strcmp(ID, niz[i].ID))
		{
			cout <<"Prosjek   " << niz[i].total_score / 4 << endl;
			return;
		}
	}
	
}

void najbolji(Studenti niz[], int size)
{
	if (!size) return;
	int najveci_indeks=0;
	for (int i = 0; i < size; i++)
	{
		if ((niz[i].total_score / 4) > (niz[najveci_indeks].total_score / 4))
			najveci_indeks = i;
	}
	cout << "Najbolji student:" << endl;
	cout << "ID:  " << niz[najveci_indeks].ID << endl;
	cout << "Ime:  " << niz[najveci_indeks].Ime << endl;
	cout << "Prezime:  " << niz[najveci_indeks].Prezime << endl;
	cout << "Spol:  " << niz[najveci_indeks].spol << endl;
	cout << "Kviz1:  " << niz[najveci_indeks].prvi_kviz << endl;
	cout << "Kviz2:  " << niz[najveci_indeks].drugi_kviz << endl;
	cout << "Midterm: " << niz[najveci_indeks].midterm_score << endl;
	cout << "Finalni: " << niz[najveci_indeks].final_score << endl;
	cout << "Totalni: " << niz[najveci_indeks].total_score << endl;

	return;
}

void najgori(Studenti niz[], int size)
{
	if (!size) return;
	int najmanji_indeks = 0;
	for (int i = 0; i < size; i++)
	{
		if ((niz[i].total_score / 4) < (niz[najmanji_indeks].total_score / 4))
			najmanji_indeks = i;
	}
	cout << "Najgori student:" << endl;
	cout << "ID:  " << niz[najmanji_indeks].ID << endl;
	cout << "Ime:  " << niz[najmanji_indeks].Ime << endl;
	cout << "Prezime:  " << niz[najmanji_indeks].Prezime << endl;
	cout << "Spol:  " << niz[najmanji_indeks].spol << endl;
	cout << "Kviz1:  " << niz[najmanji_indeks].prvi_kviz << endl;
	cout << "Kviz2:  " << niz[najmanji_indeks].drugi_kviz << endl;
	cout << "Midterm: " << niz[najmanji_indeks].midterm_score << endl;
	cout << "Finalni: " << niz[najmanji_indeks].final_score << endl;
	cout << "Totalni: " << niz[najmanji_indeks].total_score << endl;

	return;
}

void pronadi(Studenti niz[], int size)
{
	if (!size) return;
	char ID[10];
	cout << "Unesite ID za pretragu" << endl;
	cin >> ID;
	for (int i = 0; i < size; i++)
	{
		if (!strcmp(ID, niz[i].ID))
		{
			cout << "Pronaden student  " << niz[i].Ime << niz[i].Prezime << endl;
			return;
		}
	}
}

void sortiraj(Studenti niz[], int size)
{
	if (!size) return;
	Studenti temp;
	for (int i = 0; i < size; i++)
	{
		for (int j = i; j < size; j++)
		{
			if (niz[j].total_score > niz[i].total_score)
			{
				temp = niz[j];
				niz[j] = niz[i];
				niz[i] = temp;
			}
	    }
	}
}
int main()
{
	Studenti niz[100];
	int size = 0;
	int input;
	while (1)
	{
		cout << "Izbornik" << endl << "1. Dodaj Studenta" << endl << "2. Ukloni Studenta" << endl << "3. Azuriraj studenta" << endl;
		cout << "4. Prikazi sve studente" << endl << "5. Prosjek studenta" << endl << "6. Najbolji student" << endl << "7. Najgori student" << endl;
		cout << "8. Pretrazi po ID-u" << endl << "9. Sortiraj studente po bodovima" << endl << "10. Izlaz" << endl;
		cin >> input;
		switch (input)
		{
			case(1): {dodaj(niz, &size); break; }
			case(2): {ukloni(niz, &size); break; }
			case(3): {azuriraj(niz, size); break; }
			case(4): {prikazi(niz, size); break; }
			case(5): {prosjek(niz, size); break; }
			case(6): {najbolji(niz, size); break; }
			case(7): {najgori(niz, size); break; }
			case(8): {pronadi(niz, size); break; }
			case(9): {sortiraj(niz, size); break; }
			default:
				return 0;
		}
	}
}


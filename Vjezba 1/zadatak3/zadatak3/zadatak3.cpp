
#include "pch.h"
#include <iostream>

const int N = 4;


int& pronadi(int niz[N],int& indeks)
{
	int stotice, jedinice;
	for (int i = 0; i < N; i++)
	{
		stotice = (niz[i] / 100) % 10;
		jedinice = niz[i] % 10;
		if (stotice + jedinice == 5)
		{
			indeks = i;
			return niz[i];
		}
	}
}

int main()
{
	int indeks;
	int niz[N] = { 1234,5413,5565,9124 };
	pronadi(niz,indeks) += 1;
	std::cout << niz[indeks];

}
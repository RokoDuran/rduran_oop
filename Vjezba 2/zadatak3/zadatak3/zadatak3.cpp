/*
Definirati strukturu koja simulira rad vektora. Struktura se sastoji od niza int elemenata,
logicke i fiziícke veliícine niza. Fiziícka veliícina je inicijalno init, a kada se ta
veliícina napuni vrijednostima, alocira se duplo.
Napisati implementacije za funkcije vector new, vector delete, vector push back,
vector pop back, vector front, vector back i vector size.
*/
#include <iostream>

struct vektor
{
	int* niz;
	int fizi_size, logi_size;
};

vektor* vector_new()
{
	vektor* novo = new vektor;
	int i;
	std::cout << "Unesite velicinu niza\n";
	std::cin >> i;
	novo->niz = new int[i];
	novo->fizi_size = i;
	novo->logi_size = 0;
	return novo;
}

void del_ete(vektor* novo)
{
	delete[] novo->niz;
	delete novo;
}

void push_back(vektor* novo)
{
	if (novo->logi_size == novo->fizi_size)
	 {
		novo->niz = new int[2 * novo->fizi_size];
		novo->fizi_size = 2*novo->fizi_size;
	 }
	int i;
	std::cout << "Unesi broj\n";
	std::cin >> i;
	novo->niz[novo->logi_size] = i;
	novo->logi_size++;
}

void pop(vektor* novo)
{
	novo->logi_size--;
}

int& front(vektor* novo)
{
	return novo->niz[0];
}

int& back(vektor* novo)
{
	return novo->niz[novo->logi_size-1];
}

const int& sizev(vektor* novo)
	{
		return novo->logi_size;
	}
int main()
{
	vektor* gimme = vector_new();
	push_back(gimme);
	push_back(gimme);
	push_back(gimme);
	pop(gimme);
	int fronta = front(gimme);
	std::cout << "Front clan: " << fronta << "\n";
	int backa = back(gimme);
	std::cout << "Back clan: " << backa << "\n";
	int velicina = sizev(gimme);
	std::cout << "Velicina niza: " << velicina << "\n";
	del_ete(gimme);
}
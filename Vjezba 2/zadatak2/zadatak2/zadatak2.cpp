/*
Napiísite funkciju koja odvaja parne i neparne brojeve u nizu tako da se prvo pojavljuju
parni brojevi, a zatim neparni brojevi. Memoriju za niz alocirati dinamiícki. Primjer
ulaza i izlaza:
ulaz: 7 2 4 9 10 11 13 27
izlaz: 10 2 4 9 7 11 13 27
*/
#include "pch.h"
#include <iostream>


void odvoji(int niz[],int size)
{
	int neparni_counter = 0;
	for (int i = 0; i < size; i++)
	{
		if (niz[i] % 2) neparni_counter++;
	}

	int temp;
	for (int i = 0; i < size; i++)
	{
		if (niz[i] % 2)
			{
			if (i >= (size - neparni_counter));
			else 
		       	 {
				for (int j = i; j < size; j++)
				{
					if (niz[j] % 2 == 0)
					{
						temp = niz[j];
						niz[j] = niz[i];
						niz[i] = temp;
					}
				}
			     }
			}
	}
}

int main()
{
	int i;
	std::cout << "Koliko ce niz imati clanova?\n";
	std::cin >> i;
	int* niz = new int[i];
	for (int j = 0; j < i; j++)
	{
		std::cout << "Upisite " << j + 1 << ". clan: \n";

		std::cin >> niz[j];
	}
	odvoji(niz,i);
	for(int j=0; j< i;j++)
	{
		std::cout << niz[j] << "\n";
	}

}
/*Napisati funkciju koja pronalazi element koji nedostaje u cjelobrojnom rasponu 1-9.
Memoriju za niz alocirati dinamiícki, korisnik popunjava niz, a funkcija prepoznaje i
vraca modificirani sortirani niz s brojem koji nedostaje.U main funkciji ispisati dobiveni
niz i osloboditi memoriju
*/
#include "pch.h"
#include <iostream>

void sort(int niz[], int size)
{
	int temp;
	for (int i = 0; i < size; i++)
	{
		for (int j = i; j < size; j++)
		{
			if (niz[i] > niz[j])
			{
				temp = niz[j];
				niz[j] = niz[i];
				niz[i] = temp;
			}
		}
	}
}

void copy(int arr[], int size, int* dest) 
{
	for (int i = 0; i < size; i++)
		dest[i] = arr[i];
}

/*void copy_ex(int arr[], int* dest, int start, int end, int copy_to)
{
	for (int i = start; i < end; i++)
		dest[copy_to + i] = arr[i];
}
*/

void print(int* niz, int size) {
	for (int i = 0; i < size; i++) std::cout << niz[i] << ' ';
	std::cout << std::endl << std::endl;
}

int* pronadi(int niz[], int* size) {
	sort(niz, *size);
	int temp, flag = 1;
	int rupa_counter = 0;

	for (int i = 0; i < (*size); i++)
		if ((niz)[i] == (((niz)[i + 1] - 2)) && (i != (*size - 1)))
			rupa_counter++;

	int* niz2 = new int[rupa_counter + *size];
	copy(niz, *size, niz2);

	delete[] niz;

	*size += rupa_counter;

	for (int i = 0; i < *size; i++) {
		if (niz2[i] == ((niz2[i + 1] - 2)) && (i != (*size - 1))) {
			int fill = i + 1;

			for (int j = (*size) - 1; j > fill; j--)
				niz2[j] = niz2[j - 1];

			niz2[fill] = niz2[fill - 1] + 1;
		}
	}

	return niz2;
}

int main()
{
	int i;
	std::cin >> i;

	int* niz = new int[i];

	for (int j = 0; j < i; j++)
		std::cin >> niz[j];

	niz = pronadi(niz, &i);
	print(niz, i);

	delete[] niz;
	return 0;
}

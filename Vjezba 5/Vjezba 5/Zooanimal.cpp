#include "zooanimal.h"
#include <iostream>
#include <ctime>
#include <string>
#include <time.h>
#pragma warning(disable : 4996)

zooanimal::zooanimal(std::string s, std::string vrsta, int godina, int kavezi, int obroci, int vijek) : ime{ s }, vrsta{ vrsta }, godina{ godina }, broj_kaveza{ kavezi }, broj_dnevnih_obroka{ obroci }, ocekivani_vijek{ vijek }, broj_vaganja{0} {
  m.reserve(2 * vijek);
}

 zooanimal::~zooanimal() {
	m.clear();
 }

void zooanimal::copy(const zooanimal& otheranimal) {
  ime = otheranimal.ime;
  vrsta = otheranimal.vrsta;
  godina = otheranimal.godina;
  broj_kaveza = otheranimal.godina;
  broj_dnevnih_obroka = otheranimal.broj_dnevnih_obroka;
  ocekivani_vijek = otheranimal.ocekivani_vijek;

  for (unsigned int i = 0; i < otheranimal.broj_vaganja; i++) 
	  {
		m[i].godina = otheranimal.m[i].godina;
		m[i].masa = otheranimal.m[i].masa;
	  }
}

void zooanimal::uvecaj_obroke() {
	int a;

  while (std::cin >> a, a != 1 || a != 0)
  {
		if (a == 1)
		  broj_dnevnih_obroka++;
		else if (a == 0)
		  broj_dnevnih_obroka--;
  }
}

void zooanimal::podaci_mase(int godina, int masa) {
	time_t t = time(0);
	struct tm* now = localtime(&t);

	int godina_tekuca = now->tm_year + 1900;

  for(auto it = m.begin(); it != m.end(); it++) 
	{
		if ((*it).godina == godina || godina > godina_tekuca) 
		    {
				std::cout << (*it).godina << " Netocna godina.\n";
				break;
			}
    }

  m.push_back({godina, masa});
	broj_vaganja++;
}

void zooanimal::promjena_tezine() const
 {
  time_t t = time(0);
	struct tm* now = localtime(&t);
	int godina_trenutna = now->tm_year + 1900;
  
  if(broj_vaganja>=2){
	  
	if (m[broj_vaganja-2].godina==godina_trenutna-1)
		
		if (m[broj_vaganja-1].godina == godina_trenutna) {
		
			if ((m[broj_vaganja - 2].masa) > (int)(1.1*m[broj_vaganja-1].masa))
					std::cout << "Smrsavila je\n";
	
			else if ((int)(1.1*m[broj_vaganja - 2].masa) < m[broj_vaganja-1].masa)
			        std::cout << "Udebljala se\n";
														}
					 }		
  }


void zooanimal::print() const
{
	std::cout << ime << " , " << vrsta << " , " << godina << " , " << broj_kaveza << " , " << broj_dnevnih_obroka << " , " << ocekivani_vijek << std::endl;
	for(auto it=m.begin();it!=m.end();it++)
	 {
		 std::cout << (*it).godina << " , " << (*it).masa << std::endl;
	 }
}




﻿#pragma once
#include <vector>
#include <string>

struct masa{
	int masa, godina;
};

class zooanimal {
private:
  int godina,broj_kaveza,broj_dnevnih_obroka,ocekivani_vijek,broj_vaganja;

  std::string ime,vrsta;
  std::vector<masa> m;

public:
  zooanimal(std::string,std::string, int, int, int, int);
  ~zooanimal();

  void copy(const zooanimal& otheranimal);

  void uvecaj_obroke();
  void podaci_mase(int,int);

  void promjena_tezine() const;
  void print() const;
};

/* Podaci o ˇ zivotinji su:
vrsta, ime, godina rodenja, broj kaveza, broj dnevnih obroka hrane, oˇ cekivani ˇ zivotni
vijek i niz podataka o masi ˇ zivotinje koji se biljeˇ zi svaku godinu (podatak o masi je
iznos mase i godina vaganja). Niz podataka o masi treba biti dinamiˇ cki alociran u
konstruktoru, a alocirana veliˇ cina niza treba biti dovoljna za ˇ zivot dug dvostruko od
oˇ cekivanog ˇ zivotnog vijeka. Napiˇ site konstruktore i sljede´ ce member funkcije:*/

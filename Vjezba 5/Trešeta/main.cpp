// ConsoleApplication1.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include "pch.h"
#include <iostream>
#include <vector>
#include "igrac.h"
#include "Deck.h"
#include "Karta.h"

int main() {
	int n, power = 0, vrsta = 0;
	std::vector<igrac> players;

	std::cin >> n;

	for (int i = 0; i < n; i++)
	{
		std::string s;
		std::vector<Karta> k;
		getline(std::cin, s);
		players.push_back(igrac(k, s, 0, i % 2));
	}

	std::vector <Karta> vekt;
	std::vector<std::string> dummy = { "Spade","Kupe","Denari","Bastoni" };
	int ispunjeno = 0;

	while (!ispunjeno)
	{
		if (power == 7)
		{
			power += 4;
			Karta k(power, dummy[vrsta]);
			vekt.push_back(k);
		}
		else if (power == 13)
		{
			power = 0;
			if (vrsta == 3) ispunjeno = 1;
			vrsta++;
		}
		else
		{
			power++;
			Karta k(power, dummy[vrsta]);
			vekt.push_back(k);
		}
	}

	Deck d(vekt);
	d.shuffle();
	d.print_deck();








}

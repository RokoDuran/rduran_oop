#include "pch.h"
#include "Karta.h"
#include <iostream>

Karta::Karta(int p, std::string s) : power{ p }, vrsta{ s }{};

void Karta::copy(Karta other)
{
	power = other.power;
	vrsta = other.vrsta;
}

void Karta::print()const
{
	std::cout << power << " , " << vrsta << std::endl;

}
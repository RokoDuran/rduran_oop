#include "pch.h"
#include "Deck.h"
#include <vector>
#include <time.h>
#include <algorithm>
void Deck::fill_me_up(Karta& _k)
{
	vektor_karata.push_back(_k);
	capacity++;
}

void Deck::draw()
{
	capacity--;
}

void Deck::shuffle() const
    {
	std::random_shuffle(vektor_karata.begin(), vektor_karata.end());
	
	}


void Deck::print_deck() const
{
	for (int i = 0; i < capacity; i++)
	{
		vektor_karata.at(i).print();
	}
}

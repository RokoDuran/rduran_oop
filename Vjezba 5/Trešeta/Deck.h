#pragma once
#include "Karta.h"
#include <stdio.h>
#include <vector>
class Deck
{
private:
	std::vector <Karta> vektor_karata;
	int capacity;

public:
	Deck(std::vector<Karta> lol) :vektor_karata(lol), capacity(0) {};

	void fill_me_up(Karta& _k);

	void draw();

	void shuffle()const;

	void print_deck()const;
};

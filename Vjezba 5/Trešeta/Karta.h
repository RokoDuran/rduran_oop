#pragma once
#include <stdio.h>
#include <string>

class Karta
{
private:
	int power;
	std::string vrsta;

public:
	Karta();
	Karta(int, std::string);//defaultni konstruktor ionako nema koristi u ovakvom zadatku,pa nema veze sto ce nestati

	void copy(Karta drugakarta);

	void print() const ;
};
#pragma once
#include "vehicle.h"

class Land_Vehicle : public Vehicle {
protected: std::string tip;
public: std::string type()const;
		Land_Vehicle();
		Land_Vehicle(std::string);
		~Land_Vehicle();
};
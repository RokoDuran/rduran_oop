#include "pch.h"
#include "land_vehicle.h"

Land_Vehicle::Land_Vehicle() { tip = "Kopneni"; }

Land_Vehicle::Land_Vehicle(std::string tip) : tip{ tip } {};

Land_Vehicle::~Land_Vehicle() { tip.clear(); }

std::string Land_Vehicle::type()const{
	return tip;
};
#pragma once
#include "vehicle.h"

class Aircraft : virtual public Vehicle {
protected: std::string tip;
public: std::string type()const;
		Aircraft();
		Aircraft(std::string);
		~Aircraft();
};

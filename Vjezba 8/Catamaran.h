#pragma once
#include "Watercraft.h"
class Catamaran :public Watercraft {
protected: unsigned passenger;
public:
	Catamaran();
	Catamaran(int);
	unsigned passengers()const;
};
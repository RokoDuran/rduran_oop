#include "pch.h"
#include "aircraft.h"

Aircraft::Aircraft() { tip = "Zracni"; };

Aircraft::Aircraft(std::string tip) : tip{ tip } {};

Aircraft::~Aircraft() { tip.clear(); }

std::string Aircraft::type()const {
	return tip;
};
#pragma once
#include "Car.h"
#include "Ferry.h"
#include "Bike.h"
#include "Catamaran.h"
#include "Seaplane.h"

class Counter{
protected: unsigned ukupno;
public: Counter() { ukupno = 0; };
		void add(Vehicle*);
		unsigned total();
};


void Counter::add(Vehicle* v)
{
	std::cout <<"tip: " << v->type() << ", " <<"putnika: " << v->passengers();
	ukupno += v->passengers();
}

unsigned Counter::total() {
	return ukupno;
}
#pragma once
#include "land_vehicle.h"
class Car :public Land_Vehicle {
protected: unsigned  passenger = 5;
public: 
	Car();
	unsigned passengers() const;
};
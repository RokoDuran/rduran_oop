#pragma once
#include <string>
#include <iostream>
class Vehicle {
public: virtual std::string type()const = 0;
		virtual unsigned passengers()const = 0;
		virtual ~Vehicle() = 0;
};

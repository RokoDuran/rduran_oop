#include "pch.h"
#include "Watercraft.h"

Watercraft::Watercraft() { tip = "Vodeni"; }

Watercraft::Watercraft(std::string tip) : tip{ tip } {};

Watercraft::~Watercraft() { tip.clear(); }

std::string Watercraft::type()const {
	return tip;
};
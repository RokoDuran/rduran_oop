#pragma once
#include "land_vehicle.h"

class Bike :public Land_Vehicle {
protected: unsigned passenger = 1;
public:
	Bike();
	unsigned passengers()const;
};
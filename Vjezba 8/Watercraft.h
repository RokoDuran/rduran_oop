#pragma once
#include "vehicle.h"

class Watercraft : virtual public Vehicle {
protected: std::string tip;
public: std::string type()const;
		Watercraft();
		Watercraft(std::string);
		~Watercraft();
};
#pragma once
#include "watercraft.h"
#include "aircraft.h"

class Seaplane :public Watercraft,public Aircraft {
protected: unsigned passenger;

public: 
	Seaplane();
	Seaplane(unsigned);
	unsigned passengers()const;
	std::string type()const {
		std::string tip2;
		tip2=Aircraft::tip + Watercraft::tip;
		return tip2;
	};
};

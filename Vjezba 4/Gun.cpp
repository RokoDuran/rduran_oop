#include "Gun.h"

#include <iostream>

/*Weapon::Weapon()
  : m_pos{0, 0, 0}, m_capacity{6}, m_bullets{6} {}

Weapon::Weapon(int _cap, int _bullets)
  : m_pos{0, 0, 0}, m_capacity{_cap}, m_bullets{_bullets} {}
*/
Weapon::Weapon(double _x, double _y, double _z, int _cap, int _bullets)
  : m_pos{_x, _y, _z}, m_capacity{_cap}, m_bullets{_bullets} {}

void Weapon::shoot() {
  if (!is_empty()) {
    m_bullets--;
    std::cout << "Shot\n";
  } else std::cout << "No juice\n";
}

void Weapon::reload() {
  m_bullets = m_capacity;
}

int Weapon::capacity() const {
  return m_capacity;
}


int Weapon::bullets() const {
  return m_bullets;
}


bool Weapon::is_empty() const {
  return !m_bullets;
}

bool Weapon::is_full() const {
  return m_bullets;
}

void Weapon::print() const {
  std::cout << "(Capacity: " << m_capacity << ", bullets: " << m_bullets << ')';
}

#pragma once

#include "Position"

class Weapon {
 private:
  Position m_pos;

  int m_capacity;
  int m_bullets;

 public:
  Weapon();
  Weapon(int, int);
  Weapon(double, double, double, int, int);

  void shoot();
  void reload();

  int capacity() const;
  int bullets() const;

  bool is_empty() const;
  bool is_full() const;

  void print() const;

};

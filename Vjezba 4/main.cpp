#include <iostream>
#include <time.h>
//#include "pch.h"
#include "dz4_1.h"
//#include "dz4_2.h"
#include "dz4_1.cpp"
//#include "dz4_2.cpp"
#include <stdlib.h>

int main() {
  srand(time(NULL));
  Position p1;
  Position p2;
  p1.set_position(5,7,10);
  p1.print();
  p2.randomize(40,5);
  p2.print();
  std::cout<<"\n"<<p1.distance_to(p2.getduzina(),p2.getvisina(),p2.getsirina());
  return 0;
}

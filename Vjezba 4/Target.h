#pragma once

#include "Position.h"
#include "Gun.h"

class Target {
 private:
  Position m_pos;
  Weapon w;
  double m_width, m_height;
  bool m_hit;

 public:
  Target();
  Target(double, double);
  Target(double, double, double, double, double);
  
  
};

#include "Position.h"
#include <math.h>
#include <time.h>
#include <stdlib.h>

inline double rng(int _upper, int _lower) {
  return (rand() % (_upper - _lower + 1)) + _lower;
}

void Position::set_position(double _x, double _y, double _z){
  this->x=_x; this->y=_y; this->z=_z;
}


void Position::randomize(int _upper, int _lower) {
  set_position(rng(_upper, _lower), rng(_upper, _lower), rng(_upper, _lower));
}


void Position::print() const {
  std::cout << '(' << this->x << ", " << this->y << ", " << this->z << ')';
}

double Position::distance_to(double _x, double _y) const {
  return sqrt(pow(this->x - _x, 2) + pow(this->y - _y, 2));
}

double Position::distance_to(double _x, double _y, double _z) const {
  return sqrt(pow(this->x - _x, 2) + pow(this->y - _y, 2) + pow(this->z - _z, 2));
}

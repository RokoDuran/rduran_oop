#pragma once
#include <iostream>

class Position {
 private:
 // struct pos {
    double x, y, z;
  //};

  //pos m_pos;

 public:
  //Position();
  //Position(double _x, double _y, double _z);

  //void set_position(const pos& _p);
  void set_position(double _x, double _y, double _z);

  void randomize(int _upper, int _lower);

  double getduzina() const {return x;}
  double getvisina() const {return y;}
  double getsirina() const {return z;}
  const pos& get_position() const;
  void print() const;

  //double distance_to(const Position& _p) const;
  double distance_to(double _x, double _y) const;
  double distance_to(double _x, double _y, double _z) const;
};

double distance_between(const Position&, const Position&);

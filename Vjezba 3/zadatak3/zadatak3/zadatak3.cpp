#include "pch.h"
#include <iostream>
#include <string>
#include <algorithm>


std::string ocisti(std::string& recenica)
{
	std::string tempo(",. ");
	for (std::string::size_type i = 0; i < recenica.size(); ++i)
	{
		if (((recenica[i] == tempo[0] || recenica[i] == tempo[1]) && i - 1 >= 0))
		{
			if (recenica[i - 1] == tempo[2] && recenica[i + 1] == tempo[2] && i < recenica.size() - 1)
			{
				for (std::string::size_type k =i+1; k < recenica.size(); ++k)
				{
					std::swap(recenica[k], recenica[k+1]);
				}
				recenica.pop_back();
				std::swap(recenica[i - 1], recenica[i]);
			}
			else if (recenica[i - 1] == tempo[2])
				std::swap(recenica[i - 1], recenica[i]);

			else if (i <= recenica.size() - 1)
			{
				if (recenica[i + 1] != tempo[2])
				{
					recenica.push_back(tempo[2]);
					for (std::string::size_type j = recenica.size(); j > i; --j)
					{
						std::swap(recenica[j], recenica[j - 1]);
					}
				}
			}
		}

	}

	return recenica;
}

int main()
{
	std::string cleanme("Ja bih ,ako ikako mogu , ovu recenicu napisala ispravno .");
	cleanme = ocisti(cleanme);
	std::cout << cleanme << std::endl;
	std::cout << "Ja bih, ako ikako mogu, ovu recenicu napisala ispravno.";
}
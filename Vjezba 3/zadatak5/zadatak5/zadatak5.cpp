/*
5. Napisati strukturu �Producent� s atributima �name�, �movie� i �year�. Korisnik upisuje
podatke u vektor struktura Producent. Napi�si funkciju koja pronalazi najzastupljenijeg
producenta (mogu�ce je vi�se najzastupljenijih)
*/
#include "pch.h"
#include <iostream>
#include <vector>
#include <string>
#include <sstream>

class Producent {
private:
	std::string m_name;
	std::string movie;

	int year;

public:
  Producent(const std::string& _name, const std::string& _movie, int _year)
    : m_name(_name), movie(_movie), year(_year) {}

  const std::string& name() const { return m_name; }

};

void dodaj(std::vector<Producent>& v) {
  std::string name, movie;
  int year;

  do {
    std::cout << "Ime producenta:\n";
    getline(std::cin, name);

    if (name == "exit") break;

    std::cout << "Ime filma:\n";
    getline(std::cin, movie);

    std::cout << "Godina izdavanja;\n";
    std::cin >> year;

    std::cin.ignore();

    v.push_back(Producent(name, movie, year));

  } while(true);
}

bool contains(const std::vector<std::string>& v, const std::string& _s) {
  for(std::string s : v)
    if (s == _s) return true;
  return false;
}

std::string najveci(std::vector<Producent>& v) {
  int max = 0;
  int id = 0;

  std::stringstream ss;

  std::vector<std::string> names;
  std::vector<int> reps;

  for(int i = 0; i < v.size(); i++) {
    if (!contains(names, v[i].name())) {
      names.push_back(v[i].name());
      reps.push_back(1);

    } else {
      for(int j = 0; j < names.size(); j++) {
        if (names[j] == v[i].name()) {
          reps[j]++;

          if (reps[j] > max) {
            max = reps[j];
          }
        }
      }
    }
  }

  for(int i = 0; i < reps.size(); i++)
    if (reps[i] == max)
      ss << names[i] << ' ';

  return ss.str();
}

int main() {
	int i;
	std::vector<Producent> v;

  dodaj(v);
  std::cout << najveci(v) << std::endl;

  return 0;
}

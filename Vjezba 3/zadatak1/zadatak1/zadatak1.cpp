
#include "pch.h"
#include <iostream>
#include <vector>
#include <stdlib.h>
#include <time.h>

int number_generator(int a, int b)
{
	int broj = rand() % b + a;
	return broj;
}

std::vector <int> unos(int a = 0, int b = 100, int n=5, bool logic = 1) 
{
	std::vector <int> novi_vector;
	int broj;
	for (int i = 0; i < n; i++)
	{
		if (logic)
		{
			std::cin >> broj;
			//if (broj < a || broj < b) 
		    novi_vector.push_back(broj);
		}
		else  novi_vector.push_back(number_generator(a, b));
	}
	return novi_vector;


}

void ispis(std::vector <int> vektor)
{
	for (int i = 0; i < vektor.size(); i++)
	{
		std::cout << vektor[i] << std::endl;
	}
}

int main()
{
	srand((unsigned int)time(NULL));
	std::vector<int> napuni_me;
	napuni_me = unos();
	int size = sizeof(napuni_me);
	ispis(napuni_me);

}

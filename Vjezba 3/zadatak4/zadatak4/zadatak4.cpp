/*
4. Koristeci vector napravite implementaciju igre sibica gdje korisnik igra protiv racunala.
Pravila ove igre su vrlo jednostavna. Pred 2 igra�ca postavi se 21 sibica. Igraci se
izmjenjuju i uklanjaju 1, 2 ili 3 sibice odjednom. Igrac koji je prisiljen uzeti posljednju
sibicu gubi.
Korisnik unosi izbor, dok se za odabir racunala bira slucajnim izborom. Igracu se mora
dati prednost, tako da ra�cunalo prvo zapo�cinje igru.
Prije pisanja koda promislite o problemu, koji su mogu�ci slucajevi, kad je sigurna pobjeda,
kad je neizbjezan poraz (procitati �Problem sibica - pomoc�).


Pravila
Pravila ove igre su vrlo jednostavna. Za pocetak, ispred dva igraca nalazi se 21 sibica.
Igra�ci se izmjenjuju i uklanjaju 1, 2 ili 3 �sibice odjednom. Igra�c koji je prisiljen uzeti posljednju
�sibicu je izgubio.
Pojednostavite problem
U programiranju, obi�cno je losa ideja po�ceti pisati kod prije nego sto budemo imali ideju o
tome sto pokusavamo postici. Ne moramo nuzno imati na umu detaljnu strukturu ili dizajn da
bismo zapo�celi kodiranje, ali trebamo u glavi imati nekakav grubi oblik rjesenja. Za pocetak,
pojednostavimo ovaj problem onoliko koliko mozemo, a zatim od tamo napredujemo. Koji je
najjednostavniji mogu�ci slucaj?
Recimo da je nas red i da je preostala jos samo 1 �sibica. Prisiljeni smo je uzeti, i zato
gubimo.
Dalje, �sto ako postoje 2 �sibice? U tom slu�caju mo�zemo uzeti 1, i prisiliti drugog igra�ca
da uzme posljednju sibicu. Dakle, ako postoje 2 sibice, a nas je red, pobjedujemo!
3 sibice? Uzmemo 2, ostavljamo 1, i jo�s uvijek pobjedujemo.
Sto je sa 4? Uzmemo 3 �sibice, ostavivsi posljednju, ponovno pobjedujemo!
Sto se doga � da kada dodemo do 5 sibica? Ovdje smo u nevolji. Bez obzira �sto da napravimo,
nas protivnik ce zavr�siti s 2, 3 ili 4 �sibice. I znamo da od te to�cke mo�ze pobijediti (ako razmi�slja
normalno).
Sablona �
Sada mozemo vidjeti induktivni uzorak: sa samo 1 �sibicom, gubimo. Mo�zemo pobijediti
za brojanje 2, 3 i 4. Izgubili smo jo�s jednom na 5. Sada se isti uzorak ponavlja. Ako je
preostalo 6, 7 ili 8 �sibica, mo�zemo dovesti protivnika u gubitni�cku poziciju ostavljaju�ci 5
sibica. U slucaju 9 sibica, opet smo u gubitnickoj poziciji i tako dalje.
To mozemo sa�zeti formulom: Ako postoje m * 4 + 1 sibice, onda smo u gubitnickoj
poziciji. Ovdje je m cijeli broj izmedu 0 i 5, a 4 je jedan vi�se od maksimalnog broja sibica
koje igrac mo�ze uzeti u isto vrijeme. Nas cilj je stoga uvijek dovesti protivnika do sljedeceg
dostupnog od tih gubitni�ckih pozicija: 17, 13, 9, 5, 1.
Mozemo vidjeti da je igrac koji zapo�cne igru nazalost osuden na poraz, osim ako drugi
igrac napravi pogresku kod odabira broja sibica
*/
#include "pch.h"
#include <iostream>
#include <vector>
#include <stdlib.h>
#include <time.h>

bool game(std::vector<bool>& v) {
  bool player_turn = true;
  int count = 0;

  while(true) {
    int num, size = v.size();
    std::cout << "\nRound " << count++ << ": " << size << " matches left\n";

    player_turn = !player_turn;

    if (player_turn) {
      std::cout << "Player's turn...\n";
      do { std::cout << "> "; } while(std::cin >> num, !(num >= 1 && num <= 3));
    } else {
      num = rand() % 3 + 1;
      std::cout << "Computer rolls " << num << '\n';
    }

    if ((size - num) < 1)
      break;
    else if (size - num == 1) {
      player_turn = !player_turn;
      break;
    } else
      for(int i = 0; i < num; i++) v.pop_back();
  }

  return player_turn;
}

int main() {
  srand(time(NULL));

  std::vector<bool> v;
  for(int i = 0; i < 21; i++) v.push_back(true);

  bool result = game(v);

  if (result) std::cout << "\nComputer wins!\n";
  else std::cout << "\nPlayer wins!\n";

  return 0;
}

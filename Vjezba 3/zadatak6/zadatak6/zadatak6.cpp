/*
6. Korisnik unosi prirodni broj N, nakon �cega unosi N stringova u vector (duljina svakog
stringa mora biti manja od 20 - provjeriti unos, dozvoljeni su samo brojevi i velika slova
engleske abecede). Ukoliko string sadr�zava slova i brojeve, onda ga treba trasnformirati
na na�cin da se svaki podstring od 2 ili vi�se istih znakova zamijeni brojem ponavljanja
(3A = AAA). S druge strane, ukoliko string sadr�zava samo slova onda ga treba
transformirati suprotno prethodno opisanoj transformaciji (FF = 2F).
npr. N=5,
Unos: AABFBBBBCC, AABFBBBCCC AAAAFBFFFBBBCCC, 2F8H4SF
Ispis: 2ABF4B2C, 2ABF3B3C, 4AFB3F3B3C, FFHHHHHHHHSSSSF
*/
#include "pch.h"
#include <iostream>
#include <string>
#include <sstream>
#include <vector>
#include <algorithm>
#include <ctype.h>

bool check(const std::string& s) {
  for(char c : s)
    if (!isupper(c) && !isdigit(c)) return false;
  return true;
}

bool allLetters(const std::string& s) {
  for(char c : s)
    if (!isupper(c)) return false;
  return true;
}

std::vector<std::string> transform(std::vector<std::string>& v) {
  std::vector<std::string> res;
  std::stringstream ss;

  for(std::string s : v) {
    std::stringstream ss;

    bool all_letters = allLetters(s);

    if (!all_letters) {
      for(int i = 0; i < s.size(); i++) {
        if (isdigit(s[i])) {
          int rep;
          std::stringstream is;

          is << s[i];
          is >> rep;

          if (i + 1 <= s.size())
            for(int r = 0; r < rep; r++) ss << s[i + 1];

          i++;
        } else {
          ss << s[i];
        }
      }

    } else {
      for(int i = 0; i < s.size(); i++) {
        int count = 0;

        for(int j = i; j < s.size(); j++) {
          if (s[i] == s[j]) count++;
          else break;
        }

        if (count > 1) {
          ss << count;
          i += count - 1;
        }

        ss << s[i];
      }
    }

    res.push_back(ss.str());
  }

  return res;
}

int main() {
   int N;
   std::vector<std::string> v;

   std::cin >> N;

   for(int i = 0; i < N; i++) {
     std::string s;

     while(std::cin >> s, s.size() > 20 || !check(s))
       std::cout << "Uneseni string nije ispravnog formata!\n";

     v.push_back(s);
   }

   v = transform(v);

   for(int i = 0; i < v.size(); i++) {
     std::cout << v[i];
     if ((i + 1) != v.size()) std::cout << ", ";
   }

   std::cout << std::endl;

   return 0;
 }

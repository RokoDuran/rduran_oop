#include "Klase.h"
#include <vector>

void provjera_broja() {
	char broj;
	try { 
		std::cin >> broj; 
		throw Broj(broj);
	}
	catch (Broj& b) {
		
		b.Print();
	}	
}

void operatori() {
	char operatori;
	try {
		std::cin >> operatori;
		 throw Operator(operatori);
	}
	catch (Operator& o) {
		o.Print();
	}
}

int operacija() {
	char op;
	while (1) {
		std::cout << "Unesite znak operacije: \n";
		std::cin >> op;
		if ((op != '*') && (op != '+') && (op != '-') && (op != '/')) {
			std::cin.clear();
			continue;
		}
		else break;
	}
	std::cout << "Unesite prvi i drugi broj \n";
	int a, b;
	std::cin >> a >> b;
	try { throw Rezultat(a, b, op); }
	catch (Rezultat& rez) {
		rez.Print();
		return rez.rezultat;
	}
	
}

int main() {
	std::vector<int>rez;
	//provjera_broja();
	//operatori();
	//operacija();
	while (true)
	{
		rez.push_back(operacija());
		if (rez.back() == 2147483647) {
		rez.pop_back();
		break;
		}

	}

	for (int i = 0; i < rez.size(); i++)
	{
		std::cout << rez[i] << '\n';
	}
	




}

#pragma once
#include <string>
#include <iostream>
#include <fstream>

class Broj {
protected: std::string msg;
public: 
	Broj() { msg = ""; };
	Broj(char c) {
			if (!isdigit(c)) msg = "Greska, nije broj";
			else msg = "U redu";
		}
		void Print() { 
			if (msg != "")
			{
				std::ofstream file;
				file.open("errors.log", std::ios_base::out | std::ios_base::app);
				file << msg;
				file << '\n';
			}
		}
		std::string getMsg() {return msg; }
};

class Operator: public Broj{
public:
	Operator(char c) {
		if ((c != '*') && (c != '+') && (c != '-') && (c != '/')) msg = "Greska, nije operator";
		else msg = "";
	};
};


class Rezultat:public Broj {
public:
	int rezultat;
	Rezultat(int a,int b, char op) {
		switch (op) {
		case '+': rezultat=a + b; msg = ""; break;
		case '-': rezultat=a - b; msg = ""; break;
		case '*': rezultat = a * b; msg = ""; break;
		case '/': 
			if (b == 0) { msg = "Greska u jednom od operanada"; rezultat = 2147483647; break; }
			else rezultat = a / b; msg = ""; break;
		default: break;
		}
	};
};
#pragma once
#include "Zooanimal.h"
#include <iostream>
class Reptile : public Zooanimal {
protected: int vrijeme_inkubacije, temperatura_okoline;
public:
	Reptile(int vi, int to, int a, int b, int c, int d, std::string e, std::string f);
	Reptile();
	void print()const;

};
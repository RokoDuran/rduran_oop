#include "pch.h"
#include "Owl.h"
#include <iostream>

Owl::Owl(int vi, int pt, int fq, int a, int b, int c, int d, std::string e, std::string f) : kolicina_hrane{ fq }, Ptice(vi, pt, a, b, c, d, e, f) {};
Owl::Owl() : kolicina_hrane{ 0 } {};
istream& operator >> (istream& is, Owl& klasa) {
	std::cout << "Unesi vrijeme inkubacije " << std::endl;
	is >> klasa.vrijeme_inkubacije;
	std::cout << "Unesi prosjecnu temperaturu " << std::endl;
	is >> klasa.prosjecna_temperatura;
	return is;
}

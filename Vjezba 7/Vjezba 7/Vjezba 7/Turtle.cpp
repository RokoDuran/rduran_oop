#include "pch.h"
#include "Turtle.h"
#include <iostream>

Turtle::Turtle(int vi, int to, int fq, int a, int b, int c, int d, std::string e, std::string f) : kolicina_hrane{ fq }, Reptile(vi, to, a, b, c, d, e, f) {};
Turtle::Turtle() : kolicina_hrane{ 0 } {};
istream& operator >> (istream& is, Turtle& klasa) {
	std::cout << "Unesi vrijeme inkubacije " << std::endl;
	is >> klasa.vrijeme_inkubacije;
	std::cout << "Unesi temperaturu okline " << std::endl;
	is >> klasa.temperatura_okoline;
	return is;
}


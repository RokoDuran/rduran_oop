#pragma once
#include "Mammal.h"
#include <iostream>
using namespace std;
class Monkey : public Mammal {
private: int kolicina_hrane;
public:
	Monkey(int gp, int pt, int fq, int a, int b, int c, int d, std::string e, std::string f);
	Monkey();
	friend istream& operator >> (istream&, Monkey&);

};
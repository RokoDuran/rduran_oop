#pragma once
#include "Zooanimal.h"
class Mammal : public Zooanimal
{
 protected:
	 int gestacijski_period, prosjecna_temperatura;
 public:
	 Mammal();
	 Mammal(int gp, int pt, int a, int b, int c, int d, std::string e, std::string f);
	 void print() const;

};
#pragma once
#include "Mammal.h"
#include <iostream>
#include <string>
using namespace std;
class Tiger : public Mammal {
private: int kolicina_hrane;
public:
	Tiger(int gp, int pt,int fq, int a, int b, int c, int d, std::string e, std::string f);
	Tiger();
	friend istream& operator >> (istream&, Tiger&);

};
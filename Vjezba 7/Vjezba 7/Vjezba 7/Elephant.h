#pragma once
#include "Mammal.h"
#include <iostream>
using namespace std;
class Elephant : public Mammal {
private: int kolicina_hrane;
public:
	Elephant(int gp, int pt, int fq, int a, int b, int c, int d, std::string e, std::string f);
	Elephant();
	friend istream& operator >> (istream&, Elephant&);
	
};
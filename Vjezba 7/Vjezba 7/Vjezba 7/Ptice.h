#pragma once
#include "Zooanimal.h"

class Ptice : public Zooanimal {

protected: int vrijeme_inkubacije, prosjecna_temperatura;

public: 
	Ptice(int vi, int pt, int a, int b, int c, int d, std::string e, std::string f);
	Ptice();
	void print()const;

};
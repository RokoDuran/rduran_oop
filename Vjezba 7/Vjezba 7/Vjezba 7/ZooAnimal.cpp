#include "pch.h"
#include <iostream>
#include "Zooanimal.h"

Zooanimal::Zooanimal(int a, int b, int c, int d, std::string e, std::string f) : godina{ a }, broj_kaveza{ b }, broj_dnevnih_obroka{ c }, ocekivani_vijek{ d }, ime{ e }, vrsta{ f } {};
Zooanimal::Zooanimal() : godina{ 0 }, broj_kaveza{ 0 }, broj_dnevnih_obroka{ 0 }, ocekivani_vijek{ 0 }, ime{ "" }, vrsta{""} {};


std::ostream& operator <<(std::ostream& os, Zooanimal& klasa) {
	os << klasa.ime;

	os << klasa.vrsta;
	return os;
}

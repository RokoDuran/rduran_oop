#include "pch.h"
#include "Crocodile.h"
#include <iostream>

Crocodile::Crocodile(int vi, int to, int fq, int a, int b, int c, int d, std::string e, std::string f) : kolicina_hrane{ fq }, Reptile(vi, to, a, b, c, d, e, f){};
Crocodile::Crocodile() : kolicina_hrane{ 0 } {};
istream& operator >> (istream& is, Crocodile& klasa) {
	std::cout << "Unesi vrijeme inkubacije " << std::endl;
	is >> klasa.vrijeme_inkubacije;
	std::cout << "Unesi temperaturu okline " << std::endl;
	is >> klasa.temperatura_okoline;
	return is;
}


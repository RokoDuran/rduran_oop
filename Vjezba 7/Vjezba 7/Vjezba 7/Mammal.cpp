#include "pch.h"
#include "Mammal.h"
#include <iostream>
Mammal::Mammal(int gp, int pt,  int a, int b, int c, int d, std::string e, std::string f) : gestacijski_period{ gp }, prosjecna_temperatura{ pt }, Zooanimal(a,b,c,d,e,f){};
Mammal::Mammal() : gestacijski_period{ 0 }, prosjecna_temperatura{ 0 } {};

void Mammal::print() const {
	std::cout << "Gestacijski period: " << gestacijski_period << " Prosjecna temperatura: " << prosjecna_temperatura << std::endl;
}


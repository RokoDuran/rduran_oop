#pragma once
#include <string>
#include <iostream>

class Zooanimal
{
protected:
	int godina, broj_kaveza, broj_dnevnih_obroka, ocekivani_vijek;

	std::string ime, vrsta;
public: 
	Zooanimal(int,int,int,int,std::string,std::string);
	Zooanimal();
	std::string print_ime()const;
	std::string print_vrstu()const;
	friend std::ostream& operator << (std::ostream&, Zooanimal&);


};
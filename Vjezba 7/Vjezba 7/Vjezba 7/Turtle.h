#pragma once
#include "Reptile.h"
#include <iostream>
using namespace std;
class Turtle : public Reptile {
private: int kolicina_hrane;
public:
	Turtle(int vi, int to, int fq, int a, int b, int c, int d, std::string e, std::string f);
	Turtle();
	friend istream& operator >> (istream&, Turtle&);

};
#include "pch.h"
#include "Ptice.h"
#include <iostream>

Ptice::Ptice(int vi, int pt, int a, int b, int c, int d, std::string e, std::string f) : vrijeme_inkubacije{ vi }, prosjecna_temperatura{ pt }, Zooanimal(a,b,c,d,e,f) {};
Ptice::Ptice() : vrijeme_inkubacije{ 0 }, prosjecna_temperatura{ 0 } {};
void Ptice::print()const {
	std::cout << "Vrijeme inkubacije: " << vrijeme_inkubacije << " Prosjecna temperatura: " << prosjecna_temperatura << std::endl;
}


#pragma once
#include "Reptile.h"
#include <iostream>
using namespace std;
class Crocodile : public Reptile {
private: int kolicina_hrane;
public:
	Crocodile(int vi, int to, int fq, int a, int b, int c, int d, std::string e, std::string f);
	Crocodile();
	friend istream& operator >> (istream&, Crocodile&);
	
};
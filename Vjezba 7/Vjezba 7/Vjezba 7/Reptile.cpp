#include "pch.h"
#include "Reptile.h"
#include <iostream>

Reptile::Reptile(int vi, int to, int a, int b, int c, int d, std::string e, std::string f) : vrijeme_inkubacije{ vi }, temperatura_okoline{ to }, Zooanimal(a,b,c,d,e,f) {};
Reptile::Reptile() : vrijeme_inkubacije{ 0 }, temperatura_okoline{ 0 } {};

void Reptile::print()const {
	std::cout << "Vrijeme inkubacije: " << vrijeme_inkubacije << "Temperatura okline: " << temperatura_okoline << std::endl;
}


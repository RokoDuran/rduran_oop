#include "Array.h"
#include <iostream>


namespace OOP
{
	istream& operator >> (istream& is, Array& arr)
	{
		int a;

		std::cout << "size is " << arr.size << "\n";

		for (int i = 0; i < arr.size; i++)
		{
			//std::cout << is.fail() << "\n";
			is >> a;
			arr.niz[i] = a;

		}
		return is;
	}

	ostream& operator << (ostream& os, Array& arr)
	{
		for (int i = 0; i < arr.size; i++)
		{
			os << arr.niz[i];
		}
		os << "\n";
		return os;
	}
}

using namespace OOP;

int Array::counter = 0;

Array::Array(int a) : size{ a } { niz = new int[a]; counter++; }

Array::~Array() { delete[] niz; counter--; }

Array::Array(const Array& other) {
	this->niz = new int[other.size];
	std::copy(other.niz, other.niz + other.size, this->niz);
	this->size = other.size;
}



void Array::operator=(const Array& other)
{
	delete[] niz;
	niz = new int[other.size];

	std::copy(other.niz, other.niz + other.size, niz);

	size = other.size;
}

Array Array::operator+(Array& other) {
	Array novi(size + other.size);
	std::copy(niz, niz + size, novi.niz);
	std::copy(other.niz, other.niz + other.size, novi.niz + size);

	return novi;
}

Array Array::operator-(Array& other)
{
	int buduci_size = 0;
	int flag = 0;
	for (int i = 0; i < size; i++)
	{

		for (int j = 0; j < other.size; j++)
		{
			if (niz[i] == other.niz[j])
			{
				flag = 1;
				break;
			}
		}

		if (flag) flag = 0;
		else buduci_size++;
	}

	Array novi(buduci_size);
	if (buduci_size == 0) return novi;
	

	buduci_size = 0;

	for (int i = 0; i < size; i++) {
		for (int j = 0; j < other.size; j++) {
			if (niz[i] == other.niz[j]) {
				flag = 1;
				break;
			}
		}

		if (flag) flag = 0;
		else novi.niz[buduci_size++] = niz[i];
	}

	return novi;
}

bool Array::operator==(const Array& other)const {
	if (size != other.size) return false;

	for (int i = 0; i < size; i++)
		if (niz[i] != other.niz[i]) return false;

	return true;
}

bool Array::operator!=(const Array& other)const {
	return !(*this == other);
}

int& Array::operator[](int a) {
	if (a >= size || a < 0) return niz[0];

	return niz[a];
}

int Array::get_counter() {
	return counter;
}
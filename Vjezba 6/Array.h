#pragma once
#include <iostream>

/*istream&  operator >> (istream&, Array);
ostream&  operator << (ostream&, Array);*/

namespace OOP
{
	using namespace std;

	class Array
	{
	private:
		int* niz;
		int size;
		static int counter;

	public:
		//Array();
		Array(int);
		~Array();
		Array(const Array& other);
		friend istream& operator >> (istream&, Array&);
		friend ostream& operator << (ostream&, Array&);

		void operator= (const Array&);

		Array operator+ (Array&);
		Array operator- (Array&);
		bool operator== (const Array&)const;
		bool operator!= (const Array&)const;
		int& operator[] (const int);

		int get_counter();
	};




}
